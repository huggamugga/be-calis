from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Post(models.Model):
    judul = models.CharField(max_length=100)
    isi = models.TextField()
    gambar_url = models.URLField(max_length=300, blank=True)
    waktu = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    question = models.BooleanField(default=True)

    def __str__(self):
        return self.judul

class Komentar(models.Model):
    isi = models.TextField()
    gambar_url = models.URLField(max_length=300, blank=True)
    approved = models.BooleanField(default=False)
    post = models.ForeignKey("Post", on_delete=models.CASCADE)
    waktu = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.isi

class UpVote(models.Model):
    post = models.ForeignKey("Post", on_delete=models.CASCADE, blank=True, null=True)
    komentar = models.ForeignKey("Komentar", on_delete=models.CASCADE, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

class DownVote(models.Model):
    post = models.ForeignKey("Post", on_delete=models.CASCADE, blank=True, null=True)
    komentar = models.ForeignKey("Komentar", on_delete=models.CASCADE, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

class UserInfo(models.Model):
    profile_img_url = models.URLField(max_length=300, blank=True)
    full_name = models.CharField(max_length=100)
    nick_name = models.CharField(max_length=20)
    verified = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.nick_name
    