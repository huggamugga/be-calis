from django.shortcuts import render
from django.http import response, HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from .models import *
import json


# Create your views here.

def index(request):
    return HttpResponse("hai")

def feeds(request):
    all_posts = Post.objects.all().order_by('-waktu')
    
    hasil_dict = []
    for i in all_posts:
        posts_dict = {}
        posts_dict['id'] = i.id
        posts_dict['judul'] = i.judul
        posts_dict['isi'] = i.isi
        posts_dict['gambar_url'] = i.gambar_url
        posts_dict['waktu'] = i.waktu
        posts_dict['jumlah_komentar'] = Komentar.objects.filter(post=i).count()
        posts_dict['up_vote'] = UpVote.objects.filter(post=i).count()
        posts_dict['down_vote'] = DownVote.objects.filter(post=i).count()
        posts_dict['question'] = i.question
        
        posts_dict['author_id'] = i.author.id
        user_info = UserInfo.objects.get(user=i.author.id)
        posts_dict['full_name_author'] = user_info.full_name
        posts_dict['nick_name_author'] = user_info.nick_name
        posts_dict['profile_img_url'] = user_info.profile_img_url
        posts_dict['verified'] = user_info.verified
        
        if 'user_id' in request.GET:
            posts_dict['has_up_voted'] = hasUpVoted(i.id, False, request.GET['user_id'])
            posts_dict['has_down_voted'] = hasDownVoted(i.id, False, request.GET['user_id'])

        hasil_dict.append(posts_dict)
    
    if (request.GET['order'] == 'updown'):
        up_down_dict = sorted(hasil_dict, key = lambda i: (i['up_vote'] - i['down_vote']), reverse=True)
        return JsonResponse(up_down_dict, safe=False)

    return JsonResponse(hasil_dict, safe=False)

def detilPost(request, post_id):
    try:
        post = Post.objects.get(id=post_id)
        posts_dict = {}
        posts_dict['id'] = post.id
        posts_dict['judul'] = post.judul
        posts_dict['isi'] = post.isi
        posts_dict['gambar_url'] = post.gambar_url
        posts_dict['waktu'] = post.waktu
        posts_dict['jumlah_komentar'] = Komentar.objects.filter(post=post).count()
        posts_dict['question'] = post.question

        posts_dict['author_id'] = post.author.id
        user_info = UserInfo.objects.get(user=post.author.id)
        posts_dict['full_name_author'] = user_info.full_name
        posts_dict['nick_name_author'] = user_info.nick_name
        posts_dict['profile_img_url'] = user_info.profile_img_url
        posts_dict['verified'] = user_info.verified

        posts_dict['up_vote'] = UpVote.objects.filter(post=post).count()
        posts_dict['down_vote'] = DownVote.objects.filter(post=post).count()

        if 'user_id' in request.GET:
            posts_dict['has_up_voted'] = hasUpVoted(post.id, False, request.GET['user_id'])
            posts_dict['has_down_voted'] = hasDownVoted(post.id, False, request.GET['user_id'])

        list_komentar = []
        for i in Komentar.objects.filter(post=post):
            komentar = {}
            komentar['id'] = i.id
            komentar['isi'] = i.isi
            komentar['gambar_url'] = i.gambar_url
            komentar['approved'] = i.approved
            komentar['waktu'] = i.waktu
            komentar['up_vote'] = UpVote.objects.filter(komentar=i).count()
            komentar['down_vote'] = DownVote.objects.filter(komentar=i).count()

            komentar['author_id'] = i.author.id
            user_info = UserInfo.objects.get(user=i.author.id)
            komentar['full_name_author'] = user_info.full_name
            komentar['nick_name_author'] = user_info.nick_name
            komentar['profile_img_url'] = user_info.profile_img_url
            komentar['verified'] = user_info.verified

            if 'user_id' in request.GET:
                komentar['has_up_voted'] = hasUpVoted(False, i.id, request.GET['user_id'])
                komentar['has_down_voted'] = hasDownVoted(False, i.id, request.GET['user_id'])

            list_komentar.append(komentar)
        posts_dict['komentar'] = list_komentar
        posts_dict['up_vote'] = UpVote.objects.filter(post=post).count()
        posts_dict['down_vote'] = DownVote.objects.filter(post=post).count()
        return JsonResponse(posts_dict, safe=False)

    except ObjectDoesNotExist:
        return JsonResponse({}, safe=False)

@csrf_exempt
def buatPost(request):
    if request.method == 'POST':
        post = Post(judul = request.POST['judul'],
        isi = request.POST['isi'],
        gambar_url = request.POST['gambar_url'],
        question = request.POST['is_question'],
        author = User.objects.get(id=request.POST['author_id']))
        post.save()
        return JsonResponse({'message':'Berhasil membuat objek post'}, safe=False)

@csrf_exempt
def vote(request):
    if request.method == 'POST':
        if request.POST['jenis'] == 'up':
            if 'komentar_id' not in request.POST:
                if (upPost(request.POST['post_id'], request.POST['author_id'])):
                    return JsonResponse({'message':'berhasil', 'detil':'berhasil up vote post'})
                return JsonResponse({'message':'gagal','detil':'terhapus'})
            else:
                if (upKomentar(request.POST['komentar_id'], request.POST['author_id'])):
                    return JsonResponse({'message':'berhasil', 'detil':'berhasil up vote komentar'})
                return JsonResponse({'message':'gagal','detil':'terhapus'})
        else:
            if 'komentar_id' not in request.POST:
                if (downPost(request.POST['post_id'], request.POST['author_id'])):
                    return JsonResponse({'message':'berhasil', 'detil':'berhasil down vote post'})
                return JsonResponse({'message':'gagal', 'detil':'terhapus'})
            else:
                if (downKomentar(request.POST['komentar_id'], request.POST['author_id'])):
                    return JsonResponse({'message':'berhasil', 'detil':'berhasil down vote komentar'})
                return JsonResponse({'message':'gagal', 'detil': 'terhapus'})

def upPost(post_id, author_id):
    the_post = Post.objects.get(pk=post_id)
    vote_up_vote = UpVote.objects.filter(post=the_post)
    list_author_id = []
    if (len(vote_up_vote) != 0):
        for i in vote_up_vote:
            print(i.author.id)
            list_author_id.append(i.author.id)
    if (int(author_id) not in list_author_id):
        up_vote = UpVote(post = the_post, author=User.objects.get(id=author_id))
        up_vote.save()
        return True
    else:
        up_voted = UpVote.objects.get(author=author_id, post=the_post)
        up_voted.delete()
        return False

def hasUpVoted(post_id, komentar_id, watcher_id):
    if not komentar_id:
        the_post = Post.objects.get(pk=post_id)
        vote_up_vote = UpVote.objects.filter(post=the_post)
        list_voters_id = []
        if (len(vote_up_vote) != 0):
            for i in vote_up_vote:
                list_voters_id.append(i.author.id)
        if (int(watcher_id) not in list_voters_id):
            return False
        return True
    else:
        the_komentar = Komentar.objects.get(pk=komentar_id)
        vote_up_vote = UpVote.objects.filter(komentar=the_komentar)
        list_voters_id = []
        if (len(vote_up_vote) != 0):
            for i in vote_up_vote:
                list_voters_id.append(i.author.id)
        if (int(watcher_id) not in list_voters_id):
            return False
        return True

def hasDownVoted(post_id, komentar_id, watcher_id):
    if not komentar_id:
        the_post = Post.objects.get(pk=post_id)
        vote_down_vote = DownVote.objects.filter(post=the_post)
        list_voters_id = []
        if (len(vote_down_vote) != 0):
            for i in vote_down_vote:
                list_voters_id.append(i.author.id)
        if (int(watcher_id) not in list_voters_id):
            return False
        return True
    else:
        the_komentar = Komentar.objects.get(pk=komentar_id)
        vote_down_vote = DownVote.objects.filter(komentar=the_komentar)
        list_voters_id = []
        if (len(vote_down_vote) != 0):
            for i in vote_down_vote:
                list_voters_id.append(i.author.id)
        if (int(watcher_id) not in list_voters_id):
            return False
        return True


def downPost(post_id, author_id):
    the_post = Post.objects.get(pk=post_id)
    vote_down_vote = DownVote.objects.filter(post=the_post)
    list_author_id = []
    if (len(vote_down_vote) != 0):
        for i in vote_down_vote:
            list_author_id.append(i.author.id)
    if (int(author_id) not in list_author_id):
        down_vote = DownVote(post = the_post, author=User.objects.get(id=author_id))
        down_vote.save()
        return True
    else:
        up_voted = DownVote.objects.get(author=author_id, post=the_post)
        up_voted.delete()
        return False

def upKomentar(komentar_id, author_id):
    the_komentar = Komentar.objects.get(pk=komentar_id)
    vote_up_vote = UpVote.objects.filter(komentar=the_komentar)
    list_author_id = []
    if (len(vote_up_vote) != 0):
        for i in vote_up_vote:
            list_author_id.append(i.author.id)
    if (int(author_id) not in list_author_id):
        up_vote = UpVote(komentar = the_komentar, author=User.objects.get(id=author_id))
        up_vote.save()
        return True
    else:
        up_voted = UpVote.objects.get(author=author_id, komentar=the_komentar)
        up_voted.delete()
        return False

def downKomentar(komentar_id, author_id):
    the_komentar = Komentar.objects.get(pk=komentar_id)
    vote_down_vote = DownVote.objects.filter(komentar=the_komentar)
    list_author_id = []
    if (len(vote_down_vote) != 0):
        for i in vote_down_vote:
            list_author_id.append(i.author.id)
    if (int(author_id) not in list_author_id):
        down_vote = DownVote(komentar = the_komentar, author=User.objects.get(id=author_id))
        down_vote.save()
        return True
    else:
        up_voted = DownVote.objects.get(author=author_id, komentar=the_komentar)
        up_voted.delete()
        return False


@csrf_exempt
def komen(request):
    if request.method == 'POST':
        komentar = Komentar(isi = request.POST['isi'],
        gambar_url = request.POST['gambar_url'],
        approved = False,
        post = Post.objects.get(pk=request.POST['post_id']),
        author = User.objects.get(id=request.POST['author_id']))
        komentar.save()
        return JsonResponse({'message':'Berhasil membuat objek komentar'}, safe=False)

@csrf_exempt
def approveKomentar(request):
    if request.method == 'POST':
        user_info = UserInfo.objects.get(user = request.POST['author_id'])
        if user_info.verified:
            komentar = Komentar.objects.get(pk=request.POST['komentar_id'])
            komentar.approved = True
            komentar.save()
            return JsonResponse({'message':'Berhasil mengubah objek komentar menjadi approved'}, safe=False)
        else:
            return JsonResponse({'message':'Bukan merupakan Verified Account'}, safe=False)


@csrf_exempt
def authUser(request):
    if request.method == 'POST':
        user = authenticate(username = request.POST['username'], password=request.POST['password'])
        if user is not None:
            response_dict = {}
            response_dict['message'] = 'User is authenticated'
            response_dict['user_id'] = user.id
            
            user_info = UserInfo.objects.get(user=user.id)
            response_dict['user_full_name'] = user_info.full_name
            response_dict['user_nick_name'] = user_info.nick_name
            response_dict['profile_img_url'] = user_info.profile_img_url
            response_dict['verified'] = user_info.verified

            return JsonResponse(response_dict)
        else:
            return JsonResponse({'message':'Not a user'})

@csrf_exempt
def deletePost(request):
    if request.method == 'POST':
        post = Post.objects.get(id=request.POST['id'])
        if str(post.author.id) == request.POST['author_id']:
            post.delete()
            return JsonResponse({'message':'Berhasil menghapus komentar'})
        else:
            return JsonResponse({'message':'Gagal menghapus komentar, bukan pemilik post'})
