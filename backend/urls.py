from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('feeds', feeds, name='feeds'),
    path('detil/<int:post_id>', detilPost, name='detilPost'),
    path('post', buatPost, name='buatPost'),
    path('vote', vote, name='vote'),
    path('komentar', komen, name='komentar'),
    path('approve', approveKomentar, name='approve'),
    path('authUser', authUser, name='authUser'),
    path('deletePost', deletePost, name='deletePost')
]