from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Post)
admin.site.register(Komentar)
admin.site.register(UpVote)
admin.site.register(DownVote)
admin.site.register(UserInfo)